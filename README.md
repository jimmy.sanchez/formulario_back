# Formulario_Back

Este proyecto es el back-end del formulario, el controlador UserController recibe los datos del front-end guarda los datos de registro del usuario y los almacena en la base de datos.
El controlador LoginController recibe los datos del inicio de sesión, valida si el usuario y la contraseña existen en la base de datos y si es así, genera un token para la autenticacion del usuario y generar el permiso para ingresar a la aplicacion y por ultimo el controlador informationController recibe un parametro del front para mostrar la informacion del usuario que se logueó en el sistema. 



## Estrategias

Para la realización del proyecto me base en pequeños tutoriales de Angular, en consultas e información encontrada en la web.


## Dificultades

La mayor dificultad que encontré fue el poco conocimiento que tenia con respecto a las API, primera vez que trabajaba con esto, aparte de la poca documentación e información que se encuentra.

